#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    uid_t ruid = getuid();
    uid_t rgid = getgid();
    uid_t euid = geteuid();
    uid_t egid = getegid();

    printf("RUID : %i\n", ruid);
    printf("RGID : %i\n", rgid);
    printf("EUID : %i\n", euid);
    printf("EGID : %i\n", egid);

    FILE * f = fopen("mydir/mydata.txt", "r");
    char buff[255];

    if (f == NULL) {
        perror("\nCannot open file\n");
        exit(EXIT_FAILURE);
    } else {
        fgets(buff, 255, f);
        printf("\n%s\n", buff);
    }
    fclose(f);

    exit(EXIT_SUCCESS);
}
