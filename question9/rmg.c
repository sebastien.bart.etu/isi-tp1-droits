#include"check_pass.h"
#include<stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <string.h>


 struct passwd *getpwuid(uid_t uid);

int main(int argc, char **argv)
{   
    struct stat buff;
    char mot_passe[50];
    struct passwd *p;
    /*uid_t ruid;*/
    gid_t group_id;

    if (argc!=2){
        printf("le nombre d'argument n'est pas bon");
        return 1;
    }

    group_id = getgid();

    p = getpwuid(group_id);
    
    if (stat(argv[1],&buff) == (-1)){
        printf("imposible de trouver le fichier\n");
        return 1;
    }
    
    if (group_id == (buff.st_gid)){
        printf("%s entrée votre mot de passe :\n",(p->pw_name));
        scanf("%s", mot_passe);

        char login[100];
        
        format_login((p->pw_name), mot_passe, login);
        
        if(check_pass(login)){
            remove(argv[1]);
        }else{
            printf("le mot de passe que vous avez donner n'est pas bon\n");
            return 1;
        }
    }
    else{
        return 1;
    }
   return 0;
}