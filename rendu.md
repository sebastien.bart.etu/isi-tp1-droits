# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Bart Sébastien sebastien.bart.etu@univ-lille.fr

- Nom, Prénom, email: Mpemba Guillaume guillaume.mpemba@univ-lille.fr

## Question 1

Le processus peut écrire dans le fichier, puisque toto est dans le groupe ubuntu, et ubuntu a les permissions 'rw'

## Question 2

Le caractère x pour un répertoire signifie qu'on peut naviguer/rechercher à l'intérieur.
On a l'erreur Permission denied.
On peut voir tous les fichiers dans le dossier et leur type, mais on ne peut pas voir leurs droits.

## Question 3

toto@masupervm:/home/ubuntu$ ./suid 
RUID : 1001
RGID : 1000
EUID : 1001
EGID : 1000

Cannot open file
: Permission denied

ubuntu@masupervm:~$ chmod u+s suid

toto@masupervm:/home/ubuntu$ ./suid 
RUID : 1001
RGID : 1000
EUID : 1000
EGID : 1000

Salut Guigs le numéro 7

## Question 4
Nous optenons euid et egid différent de celui de ubuntu malgré le fait d'avoir set-user-id,
cela est dû aux fait que le fichier n'est pas executer il est lu puis le compiler par python3 
pour céer un fichier executable.

euid =1003
egid =1000

## Question 5

La commande chfn permet de changer les information du compte utilisateur comme le nom d'utilisateur, le
numéro de poste de travail le numéro de téléphone et ainsi de suite.

ubuntu@vm1:~$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

le fichier est accessible a la lecture et a l'execussion pour tous.
le fait que set-user-id soit positionner permet a l'utilisateur de pouvoir modifier s'est information dans 
le ficiher /etc/passwd qui n'est accessible qu'en lecture pour tous le monde.
 
-rw-r--r-- 1 root root 1889 Jan 17 20:38 /etc/passwd

c'est pour cela que lorsque l'on execute la commande en dans toto le fichier et les info de /etc/passwd sont 
bien modifier.

toto1@vm1:/home/ubuntu$ chfn
Password: 
Changing the user information for toto1
Enter the new value, or press ENTER for the default
	Full Name: 
	Room Number []: 10
	Work Phone []: 0692067406
	Home Phone []: 0692067406

toto1:x:1003:1000:,10,0692067406,0692067406:/home/toto1:/bin/bash

## Question 6

les mots de passe ne sont pas stockée dans /etc/passwd il sont /etc/shadow.
il ne sont pas dans /etc/passwd car le fichier est accessible a tous en lecture contrairement aux fichier
/etc/shadow qui n'est accessible qu'a root et aux menbre du groupe root.

## Question 7

sudo adduser -add admin5

adduser groupe_a
adduser groupe_b

sudo adduser admin5 groupe_a
sudo adduser admin5 groupe_b
 
sudo adduser -add lambda_a -ingroup  groupe_a
sudo adduser -add lambda_b -ingroup  groupe_b 

sudo adduser -add lambda_a   ubuntu
sudo adduser -add lambda_b   ubuntu

touch dir_c
touch dir_a
touch dir_b

sudo chown admin5 dir_c
sudo chown admin5:groupe_a dir_a
sudo chown admin5:groupe_b dir_b

su admin5

chmod g+s dir_c 
chmod g+s dir_a
chmod g+s dir_b

chmod +t dir_c
chmod +t dir_a
chmod +t dir_b

exit

On a besoin d'un groupe commun,soit on utilise le groupe ubuntu soit on créer un groupe_c et on
lambda_a et lambda_b aux groupe commun.

Pour lancer le script de vérification il l'exécuter en tant que root.

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

touch passwd
chmod g-r passwd
chmod o-r passwd
chmod g-w passwd

ls -l passwd
-rw------- 1 admin5 ubuntu 0 Jan 24 18:49 passwd

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








